import timeit

from csciML import read_mnist, compute_metrics
print('input --------------------------------------------')
print('Reading the data...')
X, y, classes = read_mnist()

print('prefrobnicating ----------------------------------')
from sklearn.cross_validation import train_test_split
print('Splitting the data...')
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1, train_size = 0.1, random_state=0)

# Let's convert the data from a numpy array to a pandas dataframe and compute the summary statistics.
# import pandas as pd
# pd.set_option('precision', 1)
# df = pd.DataFrame(data=X)
# print('Summary of raw features')
# print(df.describe())

# from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler
print('Scaling the data...')
sc = MinMaxScaler()
sc.fit(X_train)
X_train_std = sc.transform(X_train)
X_test_std  = sc.transform(X_test)

# print('Summary of scaled features')
# df = pd.DataFrame(X_train_std)
# print(df.describe())

# For pickling the results.
from sklearn.externals import joblib

use_knn = False
if (use_knn):
    print('kNN ----------------------------------------------')
    print('Building the classifier...')
    from sklearn.neighbors import KNeighborsClassifier
    knn = KNeighborsClassifier()
    knn.fit(X_train_std, y_train)

    print('Evaluating the classifier on the test set...')
    compute_metrics(knn, X_test_std, y_test, classes)

    print('Saving the classifier...')
    joblib.dump(knn, 'knn.pkl')

use_svc = False
if (use_svc):
    print('SVC ----------------------------------------------')
    print('Building the classifier...')
    # from sklearn.svm import SVC
    # svc = SVC()
    # svc.fit(X_train_std, y_train)


    #import GridSearch
    from sklearn.svm import SVC
    from sklearn.grid_search import GridSearchCV
    import sklearn.metrics as scores
    tuned_parameters = [
        {'kernel': ['linear'], 'C': range(1, 500, 100)},
        {'kernel': ['rbf'], 'gamma': [1, 1e-1, 1e-2, 1e-3, 1e-4], 'C': range(1, 500, 100)}]
        # {'kernel': ['poly'], 'C': range(1, 500, 10)},
        # {'kernel': ['sigmoid'], 'gamma': [1e-3, 1e-4], 'C': range(1, 500, 10)}]

    print("Starting grid search")
    svc = GridSearchCV(SVC(), tuned_parameters, scoring='recall')
    svc.fit(X_train_std, y_train)




    print('Evaluating the classifier on the test set...')
    compute_metrics(svc, X_test_std, y_test, classes)

    print('Saving the classifier...')
    joblib.dump(svc, 'svc.pkl')

use_lgr = True
if (use_lgr):
    print('Logistic regression ------------------------------')
    print('Building the classifier...')
    from sklearn.linear_model import LogisticRegression
    lgr = LogisticRegression(n_jobs=5)
    # lgr.fit(X_train_std, y_train)



    #import GridSearch
    from sklearn.svm import SVC
    from sklearn.grid_search import GridSearchCV
    import sklearn.metrics as scores
    print(lgr.get_params().keys())
    # tuned_parameters = [
        # {'kernel': ['linear'], 'C': range(1, 500, 100)},
        # {'kernel': ['rbf'], 'gamma': [1, 1e-1, 1e-2, 1e-3, 1e-4], 'C': range(1, 500, 100)}]
        # {'kernel': ['poly'], 'C': range(1, 500, 10)},
        # {'kernel': ['sigmoid'], 'gamma': [1e-3, 1e-4], 'C': range(1, 500, 10)}]
        # {'C': [0.1, 0.2, 0.3, 0.7, 1, 1.3, 2, 3]}]

    # print("Starting grid search")
    # grid = GridSearchCV(lgr, tuned_parameters, scoring='recall_micro', n_jobs=7)
    # grid.fit(X_train_std, y_train)
    #
    # print("The best classifier is: ", grid.best_estimator_)
    # print(grid.grid_scores_)
    # lgr = grid.best_estimator_
    lgr.fit(X_train_std, y_train)



    print('Evaluating the classifier on the test set...')
    compute_metrics(lgr, X_test_std, y_test, classes)

    print('Saving the classifier...')
    joblib.dump(lgr, 'lgr.pkl')
